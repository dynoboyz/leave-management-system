@component('mail::message')
# Leave Management System

Dear {{$email}},

Your leave application was {{ $status == -1 ? 'Rejected' : ($status == 1 ? 'Approved' : 'Pending') }}.

Thanks,<br>
{{ config('app.name') }}
@endcomponent