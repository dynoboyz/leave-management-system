@component('mail::message')
# Leave Management System

Dear {{$email}},

Your leave application was created & pending approval from management.

Thanks,<br>
{{ config('app.name') }}
@endcomponent