@component('mail::message')
# Leave Management System

Dear {{$email}},

Your account has been create with password {{$password}}.

Thanks,<br>
{{ config('app.name') }}
@endcomponent