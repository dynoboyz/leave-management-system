<x-app-layout>
    <x-slot name="header">
        <h2 class="p-3">
            {{ ucfirst(Auth::user()->role) }} {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div>
            <div class="bg-white">
                <div class="p-4">
                    @if (Auth::user()->role == 'superadmin')
                        <h4>User Management <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#addUserModal">+</button></h4>
                        @php
                            $authorizedRoles = ['admin', 'management', 'user'];
                            $users = \App\Models\User::whereIn('role', $authorizedRoles)->orderBy('updated_at', 'desc')->get();
                        @endphp
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th style="text-align:left">Name</th>
                                    <th style="text-align:left">Email</th>
                                    <th style="text-align:left">Role</th>
                                    <th style="text-align:left">Updated At</th>
                                    <th style="text-align:left">Action</th>
                                </tr>
                                <tbody>
                                @if ($users->isEmpty())
                                    <tr>
                                        <td colspan="8" class="text-center text-danger">-- No user registered--</td>
                                    </tr>
                                @endif
                                @foreach($users as $user)
                                    <tr>
                                        <td class="align-middle">{{ $user->name }}</td>
                                        <td class="align-middle">{{ $user->email }}</td>
                                        <td class="align-middle">{{ ucwords($user->role) }}</td>
                                        <td class="align-middle">{{ $user->updated_at }}</td>
                                        <td class="align-middle"><button type="button" class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#userModal" onClick="document.querySelector('#name').value = '{{ $user->name }}';document.querySelector('#email').value = '{{ $user->email }}';document.querySelector('.role').innerHTML = '{{ ucwords($user->role) }}';document.querySelector('#role').value = '{{ $user->role }}';">Edit</button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @elseif (Auth::user()->role == 'admin')
                        <h4>Leave Management</h4>
                        @php
                            $leave_infos = \App\Models\LeaveInfo::orderBy('updated_at', 'desc')->get();
                        @endphp
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th style="text-align:left">Email</th>
                                    <th style="text-align:left">Total Leave</th>
                                    <th style="text-align:left">Updated By</th>
                                    <th style="text-align:left">Updated At</th>
                                    <th style="text-align:left">Action</th>
                                </tr>
                                <tbody>
                                @if ($leave_infos->isEmpty())
                                    <tr>
                                        <td colspan="8" class="text-center text-danger">-- No leave info--</td>
                                    </tr>
                                @endif
                                @foreach($leave_infos as $leave_info)
                                    <tr>
                                        <td class="align-middle">{{ $leave_info->email }}</td>
                                        <td class="align-middle">{{ $leave_info->total }}</td>
                                        <td class="align-middle">{{ $leave_info->updated_by ?? 'N/A' }}</td>
                                        <td class="align-middle">{{ $leave_info->updated_at }}</td>
                                        <td class="align-middle"><button type="button" class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#leaveInfoModal" onClick="document.querySelector('#email').value = '{{ $leave_info->email }}';document.querySelector('#total').value = '{{ $leave_info->total }}';">Edit</button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @elseif (Auth::user()->role == 'management')
                        <h4>Leave Approval</h4>
                        @php
                            $leave_records = \App\Models\LeaveRecord::orderBy('updated_at', 'desc')->get();
                        @endphp
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th style="text-align:left">Email</th>
                                    <th style="text-align:left">Start Date</th>
                                    <th style="text-align:left">End Date</th>
                                    <th style="text-align:left">Leave Apply</th>
                                    <th style="text-align:left">Remarks</th>
                                    <th style="text-align:left">Status</th>
                                    <th style="text-align:left">Reason</th>
                                    <th style="text-align:left">Updated At</th>
                                    <th style="text-align:left">Action</th>
                                </tr>
                                <tbody>
                                @if ($leave_records->isEmpty())
                                    <tr>
                                        <td colspan="8" class="text-center text-danger">-- No leave record--</td>
                                    </tr>
                                @endif
                                @foreach($leave_records as $leave_record)
                                    <tr class="{{ $leave_record->status == -1 ? 'bg-danger text-white' : ($leave_record->status == 1 ? 'bg-success text-white' : '' ) }}">
                                        <td class="align-middle">{{ $leave_record->email }}</td>
                                        <td class="align-middle">{{ $leave_record->start_date }}</td>
                                        <td class="align-middle">{{ $leave_record->end_date }}</td>
                                        <td class="align-middle">{{ strval($leave_record->leave_apply) }}</td>
                                        <td class="align-middle">{{ $leave_record->remarks ?? 'N/A' }}</td>
                                        <td class="align-middle">{{ $leave_record->status == -1 ? 'Rejected' : ($leave_record->status == 1 ? 'Approved' : 'Pending' ) }}</td>
                                        <td class="align-middle">{{ $leave_record->reason ?? 'N/A' }}</td>
                                        <td class="align-middle">{{ $leave_record->updated_at }}</td>
                                        <td class="align-middle"><button type="button" class="btn btn-outline-primary btn-sm {{ $leave_record->status != 0 ? 'disabled' : '' }}" data-bs-toggle="modal" data-bs-target="#leaveRecordModal" onClick="document.querySelector('#record_id').value = '{{ $leave_record->id }}';document.querySelector('#email').value = '{{ $leave_record->email }}';document.querySelector('#start_date').value = '{{ $leave_record->start_date }}';document.querySelector('#end_date').value = '{{ $leave_record->end_date }}';document.querySelector('#leave_apply').value = '{{ $leave_record->leave_apply }}';document.querySelector('#remarks').value = '{{ $leave_record->remarks }}';document.querySelector('.status').innerHTML = '{{ $leave_record->status == -1 ? 'Rejected' : ($leave_record->status == 1 ? 'Approved' : 'Pending' ) }}';document.querySelector('#status').value = '{{ $leave_record->status }}';">Update</button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <h4>Leave Record <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#addLeaveModal">+</button></h4><div class="alert alert-primary col-md-12 d-table-cell m-4" role="alert">Total Leave: {{ \App\Models\LeaveInfo::where('email', Auth::user()->email)->first()->total }} | Pending Leave: {{ \App\Models\LeaveRecord::where('email', Auth::user()->email)->where('status', 0)->sum('leave_apply') }} | Rejected Leave: {{ \App\Models\LeaveRecord::where('email', Auth::user()->email)->where('status', -1)->sum('leave_apply') }} | Approved Leave: {{ \App\Models\LeaveRecord::where('email', Auth::user()->email)->where('status', 1)->sum('leave_apply') }} | Balance Leave: {{ (\App\Models\LeaveInfo::where('email', Auth::user()->email)->first()->total) - (\App\Models\LeaveRecord::where('email', Auth::user()->email)->where('status', 1)->sum('leave_apply')) }}</div>
                        @php
                            $leave_records = \App\Models\LeaveRecord::where('email', Auth::user()->email)->orderBy('updated_at', 'desc')->get();
                        @endphp
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th style="text-align:left">Email</th>
                                    <th style="text-align:left">Start Date</th>
                                    <th style="text-align:left">End Date</th>
                                    <th style="text-align:left">Leave Apply</th>
                                    <th style="text-align:left">Remarks</th>
                                    <th style="text-align:left">Status</th>
                                    <th style="text-align:left">Reason</th>
                                    <th style="text-align:left">Updated At</th>
                                </tr>
                                <tbody>
                                @if ($leave_records->isEmpty())
                                    <tr>
                                        <td colspan="8" class="text-center text-danger">-- No leave record--</td>
                                    </tr>
                                @endif
                                @foreach($leave_records as $leave_record)
                                    <tr class="{{ $leave_record->status == -1 ? 'bg-danger text-white' : ($leave_record->status == 1 ? 'bg-success text-white' : '' ) }}">
                                        <td class="align-middle">{{ $leave_record->email }}</td>
                                        <td class="align-middle">{{ $leave_record->start_date }}</td>
                                        <td class="align-middle">{{ $leave_record->end_date }}</td>
                                        <td class="align-middle">{{ strval($leave_record->leave_apply) }}</td>
                                        <td class="align-middle">{{ $leave_record->remarks ?? 'N/A' }}</td>
                                        <td class="align-middle">{{ $leave_record->status == -1 ? 'Rejected' : ($leave_record->status == 1 ? 'Approved' : 'Pending' ) }}</td>
                                        <td class="align-middle">{{ $leave_record->reason ?? 'N/A' }}</td>
                                        <td class="align-middle">{{ $leave_record->updated_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if (Auth::user()->role == 'superadmin')
        <div class="modal fade" id="addUserModal" tabindex="-1" aria-labelledby="addUserModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="{{ route('sa_user') }}">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="addUserModalLabel">Add User</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div>
                                <x-label for="name" :value="__('Name')" />
                                <x-input id="add_name" class="block mt-1 w-full" type="text" name="name" value="" required autofocus />
                            </div>

                            <div class="mt-4">
                                <x-label for="email" :value="__('Email')" />
                                <x-input id="add_email" class="block mt-1 w-full" type="email" name="email" value="" required />
                            </div>

                            <div class="mt-4">
                                <x-label for="role" :value="__('Role')" />
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary add_role">User</button>
                                    <input type="hidden" name="role" id="add_role" value="user"/>
                                    <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="visually-hidden">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="#" value="admin" onClick="document.querySelector('.add_role').innerHTML = 'Admin';document.querySelector('#add_role').value = 'admin';">Admin</a></li>
                                        <li><a class="dropdown-item" href="#" value="management" onClick="document.querySelector('.add_role').innerHTML = 'Management';document.querySelector('#add_role').value = 'management';">Management</a></li>
                                        <li><hr class="dropdown-divider"></li>
                                        <li><a class="dropdown-item" href="#" value="user" onClick="document.querySelector('.add_role').innerHTML = 'User';document.querySelector('#add_role').value = 'user';">User</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add user</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="userModal" tabindex="-1" aria-labelledby="userModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="{{ route('sa_store') }}">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="userModalLabel">Edit User</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div>
                                <x-label for="name" :value="__('Name')" />
                                <x-input id="name" class="block mt-1 w-full" type="text" name="name" value="" required autofocus />
                            </div>

                            <div class="mt-4">
                                <x-label for="email" :value="__('Email')" />
                                <x-input id="email" class="block mt-1 w-full bg-secondary text-white" type="email" name="email" value="" required readonly />
                            </div>

                            <div class="mt-4">
                                <x-label for="role" :value="__('Role')" />
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary role">Action</button>
                                    <input type="hidden" name="role" id="role"/>
                                    <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="visually-hidden">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="#" value="admin" onClick="document.querySelector('.role').innerHTML = 'Admin';document.querySelector('#role').value = 'admin';">Admin</a></li>
                                        <li><a class="dropdown-item" href="#" value="management" onClick="document.querySelector('.role').innerHTML = 'Management';document.querySelector('#role').value = 'management';">Management</a></li>
                                        <li><hr class="dropdown-divider"></li>
                                        <li><a class="dropdown-item" href="#" value="user" onClick="document.querySelector('.role').innerHTML = 'User';document.querySelector('#role').value = 'user';">User</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @elseif (Auth::user()->role == 'admin')
        <div class="modal fade" id="leaveInfoModal" tabindex="-1" aria-labelledby="leaveInfoModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="{{ route('a_store') }}">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="leaveInfoModalLabel">Edit User Leave</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div>
                                <x-label for="email" :value="__('Email')" />
                                <x-input id="email" class="block mt-1 w-full bg-secondary text-white" type="text" name="email" value="" readonly/>
                            </div>

                            <div class="mt-4">
                                <x-label for="total" :value="__('Total Leave')" />
                                <x-input id="total" class="block mt-1 w-full" type="total" name="total" value="" required autofocus/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @elseif (Auth::user()->role == 'management')
        <div class="modal fade" id="leaveRecordModal" tabindex="-1" aria-labelledby="leaveRecordModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="{{ route('m_store') }}">
                        @csrf
                        <input type="hidden" id="record_id" name="id"/>
                        <div class="modal-header">
                            <h5 class="modal-title" id="leaveRecordModalLabel">Leave Approval</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mt-4">
                                <x-label for="email" :value="__('Email')" />
                                <x-input id="email" class="block mt-1 w-full bg-secondary text-white" type="email" name="email" value="" required readonly />
                            </div>
                            <div class="mt-4">
                                <x-label for="start_date" :value="__('Start Date')" />
                                <x-input id="start_date" class="block mt-1 w-full bg-secondary text-white" type="start_date" name="start_date" value="" required readonly />
                            </div>
                            <div class="mt-4">
                                <x-label for="end_date" :value="__('End Date')" />
                                <x-input id="end_date" class="block mt-1 w-full bg-secondary text-white" type="end_date" name="end_date" value="" required readonly />
                            </div>
                            <div class="mt-4">
                                <x-label for="leave_apply" :value="__('Leave Apply')" />
                                <x-input id="leave_apply" class="block mt-1 w-full bg-secondary text-white" type="leave_apply" name="leave_apply" value="" required readonly />
                            </div>
                            <div class="mt-4">
                                <x-label for="remarks" :value="__('Remarks')" />
                                <x-input id="remarks" class="block mt-1 w-full bg-secondary text-white" type="remarks" name="remarks" value="" readonly />
                            </div>

                            <div class="mt-4">
                                <x-label for="status" :value="__('Status')" />
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary status">Status</button>
                                    <input type="hidden" name="status" id="status"/>
                                    <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="visually-hidden">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="#" value=1 onClick="document.querySelector('.status').innerHTML = 'Approve';document.querySelector('#status').value = 1;">Approve</a></li>
                                        <li><hr class="dropdown-divider"></li>
                                        <li><a class="dropdown-item" href="#" value=-1 onClick="document.querySelector('.status').innerHTML = 'Reject';document.querySelector('#status').value = -1;">Reject</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="mt-4">
                                <x-label for="reason" :value="__('Reason')" />
                                <x-input id="reason" class="block mt-1 w-full" type="reason" name="reason" value="" required />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @else
        <div class="modal fade" id="addLeaveModal" tabindex="-1" aria-labelledby="addLeaveModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="{{ route('u_store') }}">
                        @csrf
                        <input type="hidden" id="record_id" name="id"/>
                        <div class="modal-header">
                            <h5 class="modal-title" id="addLeaveModalLabel">Leave Application</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mt-4">
                                <x-label for="email" :value="__('Email')" />
                                <x-input id="email" class="block mt-1 w-full bg-secondary text-white" type="email" name="email" value="{{ Auth::user()->email }}" required readonly />
                            </div>
                            <div class="mt-4">
                                <x-label for="start_date" :value="__('Start Date')" />
                                <div class="date form-input col-md-8 block mt-1 w-full d-inline-block" id="start_date">
                                    <input type="text" class="form-input col-md-12 block mt-1 w-full start_date" id="date" name="start_date" onchange="document.querySelector('.start_date').value != '' && document.querySelector('.end_date').value != '' ? document.querySelector('#leave_apply').value = Math.ceil(Math.abs(new Date(document.querySelector('.end_date').value) - new Date(document.querySelector('.start_date').value) ) / (1000 * 60 * 60 * 24)) + 1 : ''"/>
                                    <span class="input-group-append">
                                        <span class="input-group-text bg-light d-none">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="mt-4">
                                <x-label for="end_date" :value="__('End Date')" />
                                <div class="date form-input col-md-8 block mt-1 w-full d-inline-block" id="end_date">
                                    <input type="text" class="form-input col-md-12 block mt-1 w-full end_date" id="date" name="end_date" onchange="document.querySelector('.start_date').value != '' && document.querySelector('.end_date').value != '' ? document.querySelector('#leave_apply').value = Math.ceil(Math.abs(new Date(document.querySelector('.end_date').value) - new Date(document.querySelector('.start_date').value) ) / (1000 * 60 * 60 * 24)) + 1 : ''"/>
                                    <span class="input-group-append">
                                        <span class="input-group-text bg-light d-none">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="mt-4">
                                <x-label for="leave_apply" :value="__('Leave Apply')" />
                                <x-input id="leave_apply" class="block mt-1 w-full bg-secondary text-white" type="leave_apply" name="leave_apply" value="" required readonly />
                            </div>
                            <div class="mt-4">
                                <x-label for="remarks" :value="__('Remarks')" />
                                <x-input id="remarks" class="block mt-1 w-full" type="remarks" name="remarks" value="" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
</x-app-layout>