<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Leave Management System</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body>
        <div class="container">
            <div class="row m-5 justify-content-center">
                <div class="text-center">
                    <div>
                        <x-application-logo class="col-md-3 fill-current text-gray-500" />
                    </div>
                    <h1 class="text-center">Leave Management System</h1>
                </div>
            </div>
            @if (Route::has('login'))
                <div class="row m-5 justify-content-center text-center">
                    @auth
                        <a href="{{ url('/dashboard') }}" class="btn btn-primary btn-lg btn-block col-md-4">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}" class="btn btn-primary col-md-4">Log in</a>
                    @endauth
                </div>
            @endif
        </div>
    </body>
</html>
