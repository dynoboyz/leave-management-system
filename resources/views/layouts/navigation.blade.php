<nav x-data="{ open: false }" class="row bg-white p-4">
    <div class="col">
        <div class="row p-3">
            <div class="col-md-9 row">
                <a href="{{ route('dashboard') }}" class="col-md-2">
                    <x-application-logo />
                </a>
                <h2 class="text-decoration-none col-md-10 mt-4">Leave Management System</h2>
            </div>

            <div class="col-md-2">
                <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>
                <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>
            </div>

            <div class="col-md-1">
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <x-button :href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-button>
                </form>
            </div>
        </div>
    </div>
</nav>
