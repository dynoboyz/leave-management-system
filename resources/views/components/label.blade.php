@props(['value'])

<label {{ $attributes->merge(['class' => 'block col-md-3']) }}>
    {{ $value ?? $slot }}
</label>
