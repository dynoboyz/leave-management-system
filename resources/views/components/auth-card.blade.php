<div class="row m-5 justify-content-center">
    <div class="bg-white col-md-4 p-5">
        <div class="col-md-4">
            {{ $logo }}
        </div>
        {{ $slot }}
    </div>
</div>
