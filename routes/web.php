<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::post('/superadmin/store', [DashboardController::class, 'superadminStore'])
                ->middleware('auth')
                ->name('sa_store');

Route::post('/superadmin/user', [DashboardController::class, 'superadminUser'])
                ->middleware('auth')
                ->name('sa_user');

Route::post('/admin/store', [DashboardController::class, 'adminStore'])
                ->middleware('auth')
                ->name('a_store');

Route::post('/management/store', [DashboardController::class, 'managementStore'])
                ->middleware('auth')
                ->name('m_store');

Route::post('/user/store', [DashboardController::class, 'userStore'])
                ->middleware('auth')
                ->name('u_store');

require __DIR__.'/auth.php';
