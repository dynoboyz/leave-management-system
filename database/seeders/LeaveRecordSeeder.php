<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\LeaveRecord;

class LeaveRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaveRecord::create([
            'email' => 'user@lms.com',
            'type' => 1,
            'start_date' => date('Y-m-d H:i:s'),
            'end_date' => date('Y-m-d H:i:s'),
            'leave_apply' => 1,
            'remarks' => 'Test apply 1 leave',
            'status' => 0,
        ]);
    }
}
