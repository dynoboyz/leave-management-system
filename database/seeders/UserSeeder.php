<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'SA_LMS',
            'email' => 'superadmin@lms.com',
            'role' => 'superadmin',
            'password' => Hash::make('Password01'),
        ]);
        User::create([
            'name' => 'A_LMS',
            'email' => 'admin@lms.com',
            'role' => 'admin',
            'password' => Hash::make('Password01'),
        ]);
        User::create([
            'name' => 'M_LMS',
            'email' => 'management@lms.com',
            'role' => 'management',
            'password' => Hash::make('Password01'),
        ]);
        User::create([
            'name' => 'U_LMS',
            'email' => 'user@lms.com',
            'role' => 'user',
            'line_manager' => 'management@lms.com',
            'password' => Hash::make('Password01'),
        ]);
    }
}
