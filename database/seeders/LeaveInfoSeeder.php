<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\LeaveInfo;

class LeaveInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaveInfo::create([
            'email' => 'user@lms.com',
            'type' => 1,
            'total' => 18,
        ]);
    }
}
