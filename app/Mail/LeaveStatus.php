<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LeaveStatus extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $status;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $status)
    {
        $this->email = $email;
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('LMS - Leave Application '.($this->status == -1 ? 'Rejected' : ($this->status == 1 ? 'Approved' : 'Pending')))
            ->markdown('emails.leave_status');
    }
}
