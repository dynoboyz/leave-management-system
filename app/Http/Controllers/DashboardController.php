<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\LeaveInfo;
use App\Models\LeaveRecord;
use App\Mail\UserCreate;
use App\Mail\LeaveStatus;
use App\Mail\LeaveApply;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    public function superadminStore(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'role' => ['required', 'string', 'max:255'],
        ]);
        
        User::where('email', $request->email)
            ->update([
                'name' => $request->name,
                'role' => $request->role,
        ]);

        return redirect(RouteServiceProvider::HOME);
    }

    public function superadminUser(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'role' => ['required', 'string', 'max:255'],
        ]);
        
        $password = substr(md5(microtime()),rand(0,26),6);

        User::create([
                'name' => $request->name,
                'email' => $request->email,
                'role' => $request->role,
                'password' => Hash::make($password),
        ]);

        LeaveInfo::create([
                'email' => $request->email,
                'total' => 18, // default 18
                'updated_by' => Auth::user()->email,
        ]);

        Log::info($request->email." => ".$password);

        Mail::to($request->email)->send(new UserCreate($request->email, $password));

        return redirect(RouteServiceProvider::HOME);
    }

    public function adminStore(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'total' => ['required', 'integer'],
        ]);
        
        LeaveInfo::where('email', $request->email)
            ->update([
                'total' => $request->total,
                'updated_by' => Auth::user()->email,
        ]);

        return redirect(RouteServiceProvider::HOME);
    }

    public function managementStore(Request $request)
    {
        $request->validate([
            'id' => ['required', 'integer'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'status' => ['required', 'integer'],
            'reason' => ['string'],
        ]);
        
        LeaveRecord::where('id', $request->id)
            ->update([
                'status' => $request->status,
                'reason' => $request->reason,
        ]);

        Mail::to(Auth::user()->email)->send(new LeaveStatus($request->email, $request->status));
        Mail::to($request->email)->send(new LeaveStatus($request->email, $request->status));

        return redirect(RouteServiceProvider::HOME);
    }

    public function userStore(Request $request)
    {
        $request->validate([
            'start_date' => ['required'],
            'end_date' => ['required'],
            'leave_apply' => ['required', 'integer'],
            'remarks' => ['string', 'max:255'],
        ]);
        
        LeaveRecord::create([
            'email' => Auth::user()->email,
            'start_date' => date("Y-m-d", strtotime($request->start_date)),
            'end_date' => date("Y-m-d", strtotime($request->end_date)),
            'leave_apply' => $request->leave_apply,
            'remarks' => $request->remarks,
        ]);

        Mail::to(Auth::user()->email)->send(new LeaveApply($request->email));

        return redirect(RouteServiceProvider::HOME);
    }
}
