## Leave Management System (LMS)

https://lms.dynoboyz.com/

### Functionality
##### General Module (100%)
- Login/Logout & Forgot Password

##### Super Admin (100%) - superadmin@lms.com
- Manage/Register users

##### Admin (100%) - admin@lms.com
- Insert/Update balance leave

##### Management (100%) - management@lms.com
- Approve/Reject leave (email notification to approval & user)

##### User (100%) - user@lms.com
- View leave balance & status
- Apply leave

### Credential

- Password01
